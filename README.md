Cstore is a customer-order liferay portlet application. The application is run within the liferay portlet container. The
application can be setup on a development machine by reading the setup section.

1.1 SetUp tools .
To set up the application the following tools are needed :

# Maven 3.0.4

      ############################# Project Configuration ########################################################
      Looking at the pom.xml file located in the root folder the following "${env.LIFERAY_HOME}" is a reference to an
      environment variable "LIFERAY_HOME". Please ensure that the variable is set to root folder of you Liferay installation

# Liferay 6.1 Community edition bundled with Tomcat-7.0.27.

    ############################# Project Configuration ########################################################
    As long as the configurations below :

            <liferay.auto.deploy.dir>${liferay.home}/deploy</liferay.auto.deploy.dir>
            <liferay.auto.deploy.tomcat.dir>${liferay.home}/tomcat-7.0.27</liferay.auto.deploy.tomcat.dir>
            <liferay.app.server.deploy.dir>${liferay.auto.deploy.tomcat.dir}/webapps</liferay.app.server.deploy.dir>
            <liferay.app.server.lib.global.dir>${liferay.auto.deploy.tomcat.dir}/lib</liferay.app.server.lib.global.dir>
            <liferay.app.server.portal.dir>${liferay.app.server.deploy.dir}/ROOT</liferay.app.server.portal.dir>

    in the pom.xml file stay the same and Liferay 6.1 is bundled with Tomcat-7.0.27 then no further configuration
    is needed for liferay.
    If you are using an external application server please set the necessary values for the properties listed above
    in the pom.xml file.


# Mysql database

    ############################# Project Configuration ########################################################
    Hibernate ORM is used to carry out all data layer abstraction please look at the hibernate.cfg.xml file to set the
    necessary parameters

    hibernate.connection.driver_class = depending on the database used please make use of the necessary driver class for
    this application mysql driver class is used.

    hibernate.connection.url = the database connection URL

    hibernate.connection.username =  The database username.

    hibernate.connection.password = Database password

    hibernate.dialect = the dialect used by hibernate to speak with the database
    for the dialect please check
    http://docs.jboss.org/hibernate/core/3.6/reference/en-US/html/session-configuration.html#configuration-optional-dialects


Deployment:

To do normal deploy , go the the root folder of the project and run
 mvn package liferay:deploy

To do direct deploy to Tomcat's webapp folder directly please run

mvn package liferay:direct-deploy

