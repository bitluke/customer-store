package com.mind.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: codemaus
 * Date: 1/29/13
 * Time: 1:05 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
public class MindOrder implements Serializable {
    private Long id;
    private String name;
    private Double price;
    private Date dateOfEntry;
    private String description;
    private MindCustomer mindCustomer;


    public MindOrder() {
    }

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Temporal(TemporalType.TIMESTAMP)
    public Date getDateOfEntry() {
        return dateOfEntry;
    }

    public void setDateOfEntry(Date dateOfEntry) {
        this.dateOfEntry = dateOfEntry;
    }

    @ManyToOne
    public MindCustomer getMindCustomer() {
        return mindCustomer;
    }

    public void setMindCustomer(MindCustomer mindCustomer) {
        this.mindCustomer = mindCustomer;
    }
}
