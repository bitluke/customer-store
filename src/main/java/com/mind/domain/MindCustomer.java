package com.mind.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: codemaus
 * Date: 1/29/13
 * Time: 1:05 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
public class MindCustomer implements Serializable{
    private Long id;
    private Long identificationNumber;
    private String firstname;
    private String secondname;
    private String telephoneNumber;
    private String address;
    private List<MindOrder> orders;



    public MindCustomer() {
    }

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(Long identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    public String getSecondname() {
        return secondname;
    }

    public void setSecondname(String secondname) {
        this.secondname = secondname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }


    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @OneToMany(cascade = {javax.persistence.CascadeType.ALL}, mappedBy = "mindCustomer")
    public List<MindOrder> getOrders() {
        return orders;
    }

    public void setOrders(List<MindOrder> orders) {
        this.orders = orders;
    }


    public void addMindOrder(MindOrder c) {
        c.setMindCustomer(this);
        getOrders().add(c);
    }

    public void removeMindOrder(MindOrder mindOrder) {
        getOrders().remove(mindOrder);
        mindOrder.setMindCustomer(null);

    }

}
