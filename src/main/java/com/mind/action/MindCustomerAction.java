package com.mind.action;

import com.mind.base.BaseDAO;
import com.mind.domain.MindCustomer;
import com.mind.domain.MindOrder;
import com.mind.service.MindCustomerDAO;
import com.mind.service.MindOrderDAO;
import org.apache.struts2.dispatcher.DefaultActionSupport;
import org.apache.struts2.interceptor.SessionAware;

import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: codemaus
 * Date: 1/29/13
 * Time: 1:28 PM
 * To change this template use File | Settings | File Templates.
 */
public class MindCustomerAction extends DefaultActionSupport  implements SessionAware {
    private List<MindCustomer> mindCustomers;
    private MindCustomer mindCustomer;
    private List<MindOrder> mindOrders;
    private MindOrder mindOrder;
    private Map session;
    private BaseDAO mindCustomerDAO = new MindCustomerDAO();

    private BaseDAO mindOrderDAO = new MindOrderDAO();


    public MindCustomerAction() {
    }

    public String findMindCustomer() {
        mindCustomer = (MindCustomer) mindCustomerDAO.getById(mindCustomer.getId());
        return SUCCESS;
    }

    public String saveMindCustomer() {
        mindCustomerDAO.create(mindCustomer);
        return SUCCESS;
    }

    public String updateMindCustomer() {
        mindCustomerDAO.edit(mindCustomer);
        return SUCCESS;
    }

    public String execute(){
       // prep();
        return SUCCESS;
    }

//    private void prep(){
//        orders = mindOrderDAO.findAll();
//        Map session = getSession();
//        session.put("orders", orders);
//    }

    public String deleteMindCustomer() {
        mindCustomer = (MindCustomer) mindCustomerDAO.getById(mindCustomer.getId());
        //mindCustomer.getOrders().removeCustomer(mindCustomer);
        mindCustomerDAO.remove(mindCustomer);
        return SUCCESS;
    }

    public String getAllMindCustomers() {
        mindCustomers = mindCustomerDAO.findAll();
        return SUCCESS;
    }


    public List<MindCustomer> getMindCustomers() {
        return mindCustomers;
    }

    public void setMindCustomers(List<MindCustomer> mindCustomers) {
        this.mindCustomers = mindCustomers;
    }

    public MindCustomer getMindCustomer() {
        return mindCustomer;
    }

    public void setMindCustomer(MindCustomer mindCustomer) {
        this.mindCustomer = mindCustomer;
    }

    public List<MindOrder> getMindOrders() {
        return mindOrders;
    }

    public void setMindOrders(List<MindOrder> mindOrders) {
        this.mindOrders = mindOrders;
    }

    public MindOrder getMindOrder() {
        return mindOrder;
    }

    public void setMindOrder(MindOrder mindOrder) {
        this.mindOrder = mindOrder;
    }

    public BaseDAO getMindCustomerDAO() {
        return mindCustomerDAO;
    }

    public void setMindCustomerDAO(BaseDAO mindCustomerDAO) {
        this.mindCustomerDAO = mindCustomerDAO;
    }

    public BaseDAO getMindOrderDAO() {
        return mindOrderDAO;
    }

    public void setMindOrderDAO(BaseDAO mindOrderDAO) {
        this.mindOrderDAO = mindOrderDAO;
    }

    public Map getSession() {
       return this.session ;
    }

    public void setSession(Map<String, Object> session) {
        this.session = session;
    }
}

