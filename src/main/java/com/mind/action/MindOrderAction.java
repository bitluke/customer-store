package com.mind.action;

import com.mind.base.BaseDAO;
import com.mind.domain.MindCustomer;
import com.mind.domain.MindOrder;
import com.mind.service.MindCustomerDAO;
import com.mind.service.MindOrderDAO;
import org.apache.struts2.dispatcher.DefaultActionSupport;
import org.apache.struts2.interceptor.SessionAware;

import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: codemaus
 * Date: 1/29/13
 * Time: 1:28 PM
 * To change this template use File | Settings | File Templates.
 */
public class MindOrderAction extends DefaultActionSupport implements SessionAware {
    private List<MindOrder> mindOrderList;
    private List<MindCustomer> mindCustomers;
    private MindOrder mindOrder;
    private Map session;
    private BaseDAO mindOrderDAO = new MindOrderDAO();
    private BaseDAO mindCustomerDAO = new MindCustomerDAO();

    public MindOrderAction() {
    }

    public String findMindOrder() {
        mindOrder = (MindOrder) mindOrderDAO.getById(mindOrder.getId());
        prep();
        return SUCCESS;
    }

    public String saveMindOrder() {
        mindOrderDAO.create(mindOrder);
        return SUCCESS;
    }

    public String updateMindOrder() {
        mindOrderDAO.edit(mindOrder);
        return SUCCESS;
    }

    public String execute(){
        prep();
        return SUCCESS;
    }

    private void prep(){
        mindCustomers = mindCustomerDAO.findAll();
        Map session = getSession();
        session.put("mindCustomers", mindCustomers);
    }


    public String deleteMindOrder() {
        mindOrder = (MindOrder) mindOrderDAO.getById(mindOrder.getId());
        mindOrder.getMindCustomer().removeMindOrder(mindOrder);
        mindOrderDAO.remove(mindOrder);
        return SUCCESS;
    }

    public String getAllMindOrder() {
        mindOrderList = mindOrderDAO.findAll();
        return SUCCESS;
    }

    public List<MindOrder> getMindOrderList() {
        return mindOrderList;
    }

    public void setMindOrderList(List<MindOrder> mindOrderList) {
        this.mindOrderList = mindOrderList;
    }

    public MindOrder getMindOrder() {
        return mindOrder;
    }

    public void setMindOrder(MindOrder mindOrder) {
        this.mindOrder = mindOrder;
    }

    public BaseDAO getMindOrderDAO() {
        return mindOrderDAO;
    }

    public void setMindOrderDAO(BaseDAO mindOrderDAO) {
        this.mindOrderDAO = mindOrderDAO;
    }


    public Map getSession() {
        return this.session ;
    }

    public void setSession(Map<String, Object> session) {
        this.session = session;
    }
}
