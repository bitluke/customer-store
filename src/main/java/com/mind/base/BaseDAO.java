package com.mind.base;

import com.mind.util.HibernateUtil;
import org.hibernate.Session;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: codemaus
 * Date: 1/29/13
 * Time: 1:01 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class BaseDAO<T> {

    private Class<T> entityClass;

    public BaseDAO(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    protected  Session getSession(){
        return HibernateUtil.getSession();
    }

    public void create(T entity) {
        getSession().getTransaction().begin();
        getSession().save(entity);
        getSession().getTransaction().commit();
    }

    public void edit(T entity) {
        getSession().getTransaction().begin();
        getSession().merge(entity);
        getSession().getTransaction().commit();
    }

    public void remove(T entity) {
        getSession().getTransaction().begin();
        getSession().delete(getSession().merge(entity));
        getSession().getTransaction().commit();
    }


    public void refresh(Object object) {
        getSession().refresh(object);
    }

    public T getById(final Long id) {
        if (id != null) {
            HibernateUtil.getSession().getTransaction().begin();
            return (T) getSession().get(entityClass, id);
        } else {
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    public List<T> findAll(int start, int resultSize) {
        getSession().getTransaction().begin();
        return getSession().createCriteria(entityClass)
                .setFirstResult(start)
                .setMaxResults(resultSize)
                .list();
    }

    @SuppressWarnings("unchecked")
    public List<T> findAll() {
        getSession().getTransaction().begin();
        return getSession().createCriteria(entityClass).list();
    }

    public void delete(T entity) {
        getSession().getTransaction().begin();
        getSession().delete(entity);
        getSession().getTransaction().commit();
    }
}