<%--
  Created by IntelliJ IDEA.
  User: codemaus
  Date: 1/28/13
  Time: 7:02 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<h2><s:if test="mindCustomer == null">Create</s:if><s:else>Update</s:else> Customer</h2>

<s:form >
    <table>
        <s:textfield name="mindCustomer.identificationNumber" label="Identity Code"/>
        <s:textfield name="mindCustomer.firstname" label="FirstName"/>
        <s:textfield name="mindCustomer.secondname" label="Surname"/>
        <s:textfield name="mindCustomer.telephoneNumber" label="Phone"/>
        <s:textfield name="mindCustomer.address" label="Address"/>
        <s:if test="mindCustomer == null">
            <s:submit action="createCustomer" value="Create"/>
        </s:if>
        <s:else>
            <s:hidden name="mindCustomer.id" />
            <s:submit action="updateCustomer" value="Update"/>
        </s:else>
    </table>
</s:form>