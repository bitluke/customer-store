<%--
  Created by IntelliJ IDEA.
  User: codemaus
  Date: 1/28/13
  Time: 7:02 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sx" uri="/struts-dojo-tags" %>
<sx:head/>

<h2><s:if test="mindOrder == null">Create</s:if>
<s:else>Update</s:else>Order</h2>

<s:form >
    <table>
        <s:textfield name="mindOrder.name" label="Name"/>
        <s:textfield name="mindOrder.price" label="Price"/>
        <s:textfield name="mindOrder.description" label="Description"/>
        <sx:datetimepicker label="Date of Entry" name="mindOrder.dateOfEntry"/>
        <s:select name="mindOrder.mindCustomer.id" listKey="id"  listValue="firstname"
                  list="#session.mindCustomers"  />
        <s:if test="mindOrder == null">
            <s:submit action="createOrder"  value="Create"/>
        </s:if>
        <s:else>
            <s:hidden name="mindOrder.id" />
            <s:submit action="updateOrder" value="Update"/>
        </s:else>

    </table>
</s:form>