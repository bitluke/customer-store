<%@ taglib prefix="s" uri="/struts-tags" %>

<h2>Create Customer</h2>

<s:form action="createCustomer">
    <table>
        <s:textfield name="customer.firstname" label="FirstName"/>
        <s:textfield name="customer.secondname" label="Surname"/>
        <s:submit value="Create"/>
    </table>
</s:form>