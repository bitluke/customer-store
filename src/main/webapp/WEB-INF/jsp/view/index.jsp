<%@ taglib prefix="s" uri="/struts-tags" %>
<s:url id="createCustomer" namespace="/edit" action="customerform"/>
<s:url id="createOrder" namespace="/edit" action="orderForm"/>
<s:url id="viewCustomer" namespace="/view" action="viewCustomer"/>
<s:url id="viewOrder" namespace="/view" action="viewOrder"/>
<strong>Customer Application</strong>

<p>
    <s:a href="%{createOrder}">Create Order</s:a><br/>
    <s:a href="%{createCustomer}">Create Customer</s:a> <br/>
    <s:a href="%{viewOrder}">View Orders</s:a><br/>
    <s:a href="%{viewCustomer}">View Customers</s:a><br/>
</p>
