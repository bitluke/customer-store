<%@ taglib prefix="s" uri="/struts-tags" %>
<s:url id="createCustomer" namespace="/edit" action="customerform"/>
<s:url id="createOrder" namespace="/edit" action="orderForm"/>
<s:url id="viewCustomer" namespace="/view" action="viewCustomer"/>
<strong>Orders</strong>
<p>
<table cellspacing="20"  cellpadding="20"  border="1">
    <thead>
    <tr>
        <td>Name</td>
        <td>Description</td>
        <td>Date of Entry</td>
        <td>Price</td>
        <td>Customer</td>
        <td colspan="2">Operations   </td>
    </tr>
    </thead>
    <s:iterator value="%{mindOrderList}" var="mindOrder">
        <s:url id="updateOrder" namespace="/edit" action="updateOrderform">
            <s:param name="mindOrder.id" value="%{id}" />
        </s:url>

        <s:url id="deleteOrder" portletUrlType="action" namespace="/edit" action="deleteOrder">
            <s:param name="mindOrder.id" value="%{id}" />
        </s:url>

        <tr>
            <td><s:property value="%{name}"/></td>
            <td><s:property value="%{description}"/></td>
            <td><s:date name="%{dateOfEntry}"/></td>
            <td><s:property value="%{price}"/></td>
            <td><s:property value="%{mindCustomer.firstname}"/></td>
            <td><s:a href="%{updateOrder}" >edit</s:a></td>
            <td><s:a href="%{deleteOrder}" >delete</s:a></td>
        </tr>
    </s:iterator>
</table>
</p>
<span><s:a href="%{createOrder}" >Create Order</s:a></span> | <span><s:a href="%{createCustomer}" >Create Customer</s:a> </span>  | <span><s:a href="%{viewCustomer}" >View Customer</s:a> </span>