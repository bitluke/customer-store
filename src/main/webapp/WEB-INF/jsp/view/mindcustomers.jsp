<%@ taglib prefix="s" uri="/struts-tags" %>
<s:url id="createCustomer" namespace="/edit" action="customerform"/>
<s:url id="createOrder" namespace="/edit" action="orderForm"/>
<s:url id="viewOrder" namespace="/view" action="viewOrder"/>

<strong>Customers</strong>
<p>
<table cellspacing="20" cellpadding="20" border="1">
    <thead>
    <tr>
        <td>Id Number</td>
        <td>First Name</td>
        <td>Second Name</td>
        <td>Phone</td>
        <td>Address</td>
        <td colspan="2">Operations    </td>
    </tr>
    </thead>
    <s:iterator value="%{mindCustomers}" var="mindcustomer">

        <s:url id="updateCustomer" namespace="/edit" action="updateCustomerform">
            <s:param name="mindCustomer.id" value="%{id}" />
        </s:url>

        <s:url id="deleteCustomer" portletUrlType="action" namespace="/edit" action="deleteCustomer">
            <s:param name="mindCustomer.id"  value="%{id}" />
        </s:url>

        <tr>
            <td><s:property value="%{identificationNumber}"/></td>
            <td><s:property value="%{firstname}"/></td>
            <td><s:property value="%{secondname}"/></td>
            <td><s:property value="%{telephoneNumber}"/></td>
            <td><s:property value="%{address}"/></td>
            <td><s:a href="%{updateCustomer}" >edit</s:a></td>
            <td><s:a href="%{deleteCustomer}" >delete</s:a></td>
        </tr>
    </s:iterator>
</table>
</p>
<span><s:a href="%{createOrder}" >Create Order</s:a> </span> | <span><s:a href="%{createCustomer}" >Create Customer</s:a> </span> | <span><s:a href="%{viewOrder}" >View Orders</s:a> </span>